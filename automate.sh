#!/bin/sh
# depth 1 - user
mkdir "$1 at `date`"
cd "$1 at `date`"
mkdir {about_me,my_friends,my_system_info}
#depth 2 - user > about_me
cd about_me
mkdir {personal,professional}
#depth 3 - about_me > personal
cd personal 
echo “https://www.facebook.com/$2“ > facebook.txt
chmod u+x facebook.txt
#depth 2 - about_me
cd ..
#depth 3 - about_me > professional
cd professional
echo "https://www.linkedin.com/in/$3" > linkedin.txt
chmod u+x linkedin.txt
cd ..
cd ..
#depth 3 - my_friends
cd my_friends
curl https://gist.githubusercontent.com/tegarimansyah/e91f335753ab2c7fb12815779677e914/raw/94864388379fecee450fde26e3e73bfb2bcda194/list%2520of%2520my%2520friends.txt -o list_of_my_friend.txt
cd ..
#depth 3 - my_system_info
cd my_system_info
echo "My username: `uname`\nWith host: `uname -a`" > about_this_laptop.txt
chmod u+x about_this_laptop.txt
ping -c 3 www.google.com > internet_connection.txt
chmod u+x internet_connection.txt
cd ..