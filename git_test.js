const prompt = require('prompt-sync')({sigint: true});

const randomize = (max,min) =>{
    return Math.floor.random*(max-min)+min;
}

const getRandomNumber = () => {
    let max = prompt("MAX VALUE IS : ")
    let min = prompt("MIN VALUE IS : ")
    console.log("RANDOM NUMBER : ")
    console.log(randomize(max,min))
}

const addNumber = (x,y) => {
    return x+y
}

const modNumber = (x,y) => {
    return x%y
}

getRandomNumber()